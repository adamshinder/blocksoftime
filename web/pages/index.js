import Head from 'next/head';
import styles from '../styles/Home.module.css';
import classnames from 'classnames';

//components
import Header from 'components/Home/Header';
import Banner from 'components/Home/Banner'; //The do you have an idea for a smart contract section


import data from 'data/data.json';
import examplesData from 'data/examples.json';

export default function Home() {
  return (
    <div className={classnames('border-t-6 border-blue-800', styles.container)}>
      <Head>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1"
        />
        <meta
          name="description"
          content="Welcome to Blocks Of Time, blockchain timecapsule."
        />
      </Head>

      <Header
        title={data.header.title}
        description={data.header.desc}
        description2={data.header.desc2}
        buttonLink={data.header.button.link}
        buttonLabel={data.header.button.label}
      />



      <Banner banner={data.banner} />

    </div>
  );
}

// <Section title={data.projects.title}>
// <Projects projects={examplesData} />
// </Section>
//<link rel="icon" href="/logo_blue.svg" />