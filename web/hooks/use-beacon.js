import { useCallback, useState } from 'react';

import { TezosToolkit } from '@taquito/taquito';

export const RPC_URL = 'https://fierce-eyrie-57334.herokuapp.com/https://testnet-tezos.giganode.io'; //https://cors-anywhere.herokuapp.com

const options = {
  name: 'Blocks Of Time',
};

export const Tezos = new TezosToolkit(RPC_URL);

export default function useBeacon() {
  const [pkh, setUserPkh] = useState();
  const connect = useCallback(async () => {

    //console.log('pkh==>>>', pkh)

    /*fetch("https://testnet-tezos.giganode.io/chains/main/blocks/head/context/contracts/KT1GpGZF6ecLRxt9xwGuC9o8VKezb3rgZjb3/script", requestOptions)
     .then(response => response.text())
     .then(result => console.log(result))
     .catch(error => console.log('error +++', error));
    
       // setUserPkh(await wallet.getPKH());
     }, []);
    
     return { connect, pkh };    */

    const { BeaconWallet } = await import('@taquito/beacon-wallet');
    const wallet = new BeaconWallet(options);

    Tezos.setProvider({ wallet });

    await wallet.requestPermissions({
      network: {
        type: 'hangzhounet',
        rpcUrl: RPC_URL,
      },
    });

    setUserPkh(await wallet.getPKH());
  }, []);

  return { connect, pkh };
}



//var myHeaders = new Headers();
//myHeaders.append("Cookie", "giganode_sticky=5630655dc18cd7863195adc6b66ee141");
//myHeaders.append("Access-Control-Allow-Origin", "*");
//
//var requestOptions = {
//  method: 'GET',
//  headers: myHeaders,
//  redirect: 'follow'
//};
//
//fetch("https://testnet-tezos.giganode.io/chains/main/blocks/head/context/contracts/KT1GpGZF6ecLRxt9xwGuC9o8VKezb3rgZjb3/script", requestOptions)
//  .then(response => response.text())
//  .then(result => console.log(result))
//  .catch(error => console.log('error +++', error));
//
//    // setUserPkh(await wallet.getPKH());
//  }, []);
//
//  return { connect, pkh };
//}
