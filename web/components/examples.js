import dynamic from 'next/dynamic';

const examples = {
  'grocery-list': dynamic(() => import('./GroceryList')),
};

export default examples;
