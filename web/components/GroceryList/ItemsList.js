import PropTypes from 'prop-types';
import Loader from 'react-loader-spinner';

function ItemsList({
  itemsList,
  deleteItem,
  updateItem,
  isLoading,
  setItemsList,
}) {
  return (
    <div className="container mx-auto p-10 pt-0">
      <div className="w-2/3 mx-auto">
        {!isLoading ? (
          <>
            <div className="items-list-section">
              {itemsList.length !== 0 ? (
                itemsList.map((item, index) => {
                  return (
                    <div
                      key={item.title}
                      className="item border rounded-md mb-5 px-4 py-2 bg-gray-100 flex justify-between items-center"
                    >
                      <div className="item-info">
                        <div className="item-info">
                          <b>Title: </b> {item.title}
                        </div>
                        <div className="item-info">
                          <b>Description: </b>
                          <input
                            className={`p-1 ${
                              item.editMode ? 'bg-white' : 'bg-gray-100'
                            }`}
                            defaultValue={item.desc}
                            onChange={(e) =>
                              changeDesc(e.target.value, index)
                            }
                          />
                        </div>
                      </div>
                      <div className="item-actions">
                        {item.editMode ? (
                          <>
                            <button
                              className="btn bg-yellow-500 rounded-md px-4 py-2 text-white text-sm mr-2"
                              onClick={() => {
                                toggleEditMode(false, index);
                              }}
                            >
                              cancel
                            </button>
                            <button
                              className="btn bg-green-500 rounded-md px-4 py-2 text-white text-sm mr-2"
                              onClick={() => {
                                toggleEditMode(false, index);
                                updateItem(item);
                              }}
                            >
                              save
                            </button>
                          </>
                        ) : (
                          <button
                            className="btn bg-blue-500 rounded-md px-4 py-2 text-white text-sm mr-2"
                            onClick={() => toggleEditMode(true, index)}
                          >
                            Edit {item.editMode}
                          </button>
                        )}

                        <button
                          className="btn bg-red-500 rounded-md px-4 py-2 text-white text-sm"
                          onClick={() => deleteItem(item.title)}
                        >
                          Delete
                        </button>
                      </div>
                    </div>
                  );
                })
              ) : (
                <div className="text-center text-lg font-bold text-gray-400">
                  Storage is empty
                </div>
              )}
            </div>
          </>
        ) : (
          <div className="text-center flex flex-col justify-center items-center fixed top-0 bottom-0 left-0 right-0 bg-gray-100 bg-opacity-25	">
            <Loader
              type="TailSpin"
              color="#cacaca"
              height={50}
              width={50}
              className="mb-3"
            />
            Fetching Storage
          </div>
        )}
      </div>
    </div>
  );

  async function changeDesc(newValue, index) {
    itemsList[index].newDesc = newValue;
  }

  async function toggleEditMode(status, index) {
    await setItemsList([]);
    itemsList[index].editMode = status;
    await setItemsList(itemsList);
  }
}

export default ItemsList;

ItemsList.propTypes = {
  itemsList: PropTypes.arrayOf(
    PropTypes.shape({
      editMode: PropTypes.bool,
      newDesc: PropTypes.string,
      name: PropTypes.string,
    })
  ).isRequired,
  deleteItem: PropTypes.func.isRequired,
  updateItem: PropTypes.func.isRequired,
  isLoading: PropTypes.bool,
  setItemsList: PropTypes.func.isRequired,
};
