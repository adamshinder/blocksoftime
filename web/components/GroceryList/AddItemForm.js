import PropTypes from 'prop-types';
import { useState } from 'react';

export default function AddItemForm({ error, adding, onSubmit, setError }) {
  const [title, setTitle] = useState('');
  const [desc, setDesc] = useState('');

  return (
    <div className="container mx-auto px-10 py-10">
      <div className="w-2/3 mx-auto">
        <div
          className={`add-item-form mb-8 flex px-5 py-5 rounded-md	justify-between border`}
        >
          <div className="input-field w-full mr-2">
            <input
              type="text"
              className="border py-2 px-2 rounded-md  w-full"
              value={title}
              onChange={(e) => {
                setTitle(e.target.value);
              }}
              placeholder="Title"
            />
          </div>
          <div className="input-field w-full mr-2">
            <input
              type="text"
              className="border py-2 px-2 rounded-md  w-full"
              value={desc}
              onChange={(e) => setDesc(e.target.value)}
              placeholder="Description "
            />
          </div>
          <div className="button-field">
            <button
              className={`btn bg-green-500 rounded-md px-4 py-2 text-white ${
                adding ? 'disabled' : ''
              }`}
              onClick={async () => {
                await onSubmit(title, desc);
                cleanInputs();
              }}
              disabled={adding}
            >
              {adding ? 'Engraining...' : 'Engrain'}
            </button>
          </div>
        </div>

        {error && (
          <div className="flex px-8 py-3 mb-6 rounded-md	justify-between  bg-red-100 text-red-600">
            {error}
          </div>
        )}
      </div>
    </div>
  );

  function cleanInputs() {
    setTitle('');
    setDesc('');
  }
}

AddItemForm.propTypes = {
  error: PropTypes.string,
  adding: PropTypes.bool,
  onSubmit: PropTypes.func.isRequired,
  setError: PropTypes.func.isRequired,
};
