import { TezosToolkit, MichelsonMap } from '@taquito/taquito';

import PropTypes from 'prop-types';
import { Tezos } from 'hooks/use-beacon';
import { useState, useCallback, useEffect } from 'react';
import Layout from 'components/Layout';

//import { BigNumber } from 'bignumber.js';


import ItemsList from './ItemsList';
import AddItemForm from './AddItemForm';
//import { keyToRuntime } from 'node_modules/next/dist/compiled/webpack/bundle5';

export default function GroceryList({ contractAddress, userAddress }) {
  const [itemsList, setItemsList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);
  const [adding, setAdding] = useState(false);

  const getStorage = useCallback(
    async function getStorage() {
      setIsLoading(true);
      try {
        setItemsList([]);
        const contract = await Tezos.wallet.at("KT1UMWxAK9wUDjdVdB4QGZBLdWbDCovwAiax");
        // const storage = MichelsonMap(string, BigNumber)
        const storage = await contract.storage();//const storage = await MichelsonMap.fromLiteral({});  //

        const bigmapStorage = () => {
          return `https://api.hangzhou2net.tzkt.io/v1/bigmaps/127689/keys?active=true&offset=0&limit=50 `;
        }
        const bigstorageFetch = () => {
          fetch(bigmapStorage()).then((response) =>
            response.json().then((jsonData) => {
              setBuy(jsonData);
              console.log(jsonData);
            })
          );
        };
        await bigstorageFetch;
        console.log(bigstorageFetch);

        setItemsList(
          //Array.from(storage.get()) ==> ({
          Array.from(storage.entries()).big_map(([key, value]) => ({ //changed map to big_map
            key,
            value,
          }))
        );
      } catch (error) {
        setError(error.message);
      }

      setIsLoading(false);
    },
    [contractAddress]
  );

  useEffect(() => {
    getStorage();
  }, [getStorage]);

  return (
    <Layout
      userAddress={userAddress}
      contractAddress={contractAddress}
      title="Blocks Of Time"
    >
      <AddItemForm
        contractAddress={contractAddress}
        itemsList={itemsList}
        getStorage={getStorage}
        error={error}
        setError={setError}
        adding={adding}
        onSubmit={onSubmit}
      />
      <ItemsList
        itemsList={itemsList}
        deleteItem={deleteItem}
        updateItem={updateItem}
        isLoading={isLoading}
        setItemsList={setItemsList}
      />

    </Layout>
  );

  async function onSubmit(title, desc) {
    if (!title && !desc) {
      return;
    }

    const hasItem = itemsList.some(
      (item) => item.name.toLowerCase() === title.toLowerCase()
    );

    setError(null);

    if (hasItem) {
      setError('Input ' + title + ' already exists.');
      return;
    }

    setAdding(true);

    try {
      const contractInstance = await Tezos.wallet.at(contractAddress);
      const operation = await contractInstance.methods
        .createItem(title, desc)
        .send();
      await operation.confirmation();
      getStorage();
    } catch (error) {
      setError(error.message);
    }
    setAdding(false);
  }

  async function deleteItem(item) {
    setIsLoading(true);
    try {
      const contractInstance = await Tezos.wallet.at(contractAddress);
      const operation = await contractInstance.methods.removeItem(item).send();
      await operation.confirmation();
      getStorage();
    } catch (error) {
      setError(error.message);
    }
    setIsLoading(false);
  }

  async function updateItem(item) {
    if (!item.newDesc) {
      return;
    }
    setIsLoading(true);
    try {
      const contractInstance = await Tezos.wallet.at(contractAddress);
      const operation = await contractInstance.methods
        .changeDesc(item.name, item.newDesc)
        .send();
      await operation.confirmation();
      getStorage();
    } catch (error) {
      setError(error.message);
    }

    setIsLoading(false);
  }
}

GroceryList.propTypes = {
  contractAddress: PropTypes.string.isRequired,
  userAddress: PropTypes.string.isRequired,
};
