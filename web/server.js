const express = require('express');
const axios = require('axios');
const cors = require('cors');

const app = express();
app.use(cors());

app.post('/some-route', (req, res) => {
    try {
        const { data } = await axios.get(`https://testnet-tezos.giganode.io/chains/main/blocks/head/context/contracts/KT1GpGZF6ecLRxt9xwGuC9o8VKezb3rgZjb3/script`);;
        console.log('data==>>', data);
        res.send(data);
    } catch (err) {
        console.log('an errr occured==>>', err);
        res.status(500).send(err);
    }
});

const port = process.env.PORT || 3001;

app.listen(port, () => console.log('server started on ', port));